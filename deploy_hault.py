#!/usr/bin/env python
# coding: utf-8

# In[373]:


import pandas as pd
import numpy as np


# In[374]:


df1 = pd.read_csv('input/bokaro_inv.csv')


# In[375]:


df1.head()


# In[376]:


len(df1.invoiceNo.unique())


# In[377]:


df1.shape


# In[378]:


df2 = pd.read_csv("input/bokaro_tel.csv")


# In[379]:


df2.shape


# In[380]:


df2.head()


# In[381]:


len(df2.invoiceNumber.unique())


# In[382]:


df2['startEntryDatetimestamp'] = pd.to_datetime(df2['startEntryDatetimestamp'])


# In[383]:


df2 = df2[df2['invoiceNumber'] != 'tripsanuy']


# In[384]:


df2['invoiceNumber'] = df2['invoiceNumber'].astype(np.int64)


# In[385]:


len(df2.invoiceNumber.unique())


# In[386]:


df1['createdDateTime'] = pd.to_datetime(df1['createdDateTime'])


# In[387]:


df2['createDatetimestamp'] = pd.to_datetime(df2['createDatetimestamp'])


# In[388]:


df3 = pd.merge(df1, df2, left_on = 'invoiceNo',right_on = 'invoiceNumber', how ='inner')


# In[389]:


df3.shape


# In[390]:


len(df3.invoiceNumber.unique())


# In[391]:


from geopy.distance import great_circle


# In[392]:


b=[]
for u,v,w,x in zip(df3.shipToLat, df3.shipToLong, df3.latitude, df3.longitude):
    b.append(great_circle((u,v),(w,x)).km)


# In[393]:


df3['distance'] = np.nan


# In[394]:


for col in df3.columns:
    df3['distance'].values[:] = b


# In[395]:


def buckets(x):
    if x<=3:
        return '3 - 0'
    elif x<=10:
        return '10 - 3'
    elif x<=30:
        return '30 - 10'
    elif x<=40:
        return '40 - 30'
    else:
        return '> 40'
        


# In[396]:


df3['buckets'] = df3['distance'].apply(lambda x: buckets(x))


# In[397]:


df3


# In[151]:


#df3.to_csv("hault_try.csv")


# In[398]:


count=0
z = []
for i in range(0, len(df3) - 1):
    if (df3['startEntryDatetimestamp'][i] != df3['startEntryDatetimestamp'][i+1]) & (df3['distance'][i] == df3['distance'][i+1]):
        z.append(str(df3['invoiceNo'][i])+ ", "+ str(df3['invoiceNo'][i+1])+ ", "+ str(df3['buckets'][i])  + ", "+ str(df3['buckets'][i+1]) + ", " + str(df3['startEntryDatetimestamp'][i+1] - df3['startEntryDatetimestamp'][i]))
        count+=1


# In[399]:


print(count)


# In[400]:


z


# In[401]:


len(z)


# In[402]:


a = [i.split(',',1)[0] for i in z]


# In[403]:


b = [i.split(',',2)[1] for i in z]


# In[404]:


c = [i.split(',',3)[2] for i in z]


# In[405]:


d = [i.split(',',4)[3] for i in z]


# In[406]:


e = [i.split(',',5)[4] for i in z]


# In[407]:


temp = pd.DataFrame({'invoice1':a,'invoice2':b, 'bucket1':c,'bucket2':d,'time':e})


# In[408]:


temp


# In[409]:


temp['bucket2'] = temp['bucket2'].apply(lambda x:x.lstrip(" "))


# In[410]:


temp['bucket1'] = temp['bucket1'].apply(lambda x:x.lstrip(" "))


# In[411]:


temp['invoice2'] = pd.to_numeric(temp['invoice2']) 


# In[412]:


temp['invoice1'] = pd.to_numeric(temp['invoice1']) 


# In[413]:


temp.shape


# In[414]:


temp


# In[415]:


camp =0
x=0
for i in range(0, len(temp)):
    x+=1
    if(temp['invoice1'][i] != temp['invoice2'][i]) | (temp['bucket1'][i] != temp['bucket2'][i]):
        temp.drop(i, inplace=True)
        camp+=1
        


# In[416]:


x


# In[417]:


count


# In[418]:


camp


# In[419]:


temp


# In[420]:


temp['time_hrs'] = temp['time']/np.timedelta64(1,'h')


# In[421]:


temp


# In[422]:


temp = temp.drop(['invoice2', 'bucket2'], axis =1)


# In[423]:


temp


# In[424]:


temp['time_sum'] = temp.groupby(['invoice1', 'bucket1'])['time_hrs'].transform('sum')


# In[425]:


temp


# In[426]:


temp = temp.drop_duplicates(['invoice1','bucket1'])


# In[427]:


temp = temp.reset_index()


# In[428]:


temp = temp.drop('index', axis =1)


# In[429]:


temp


# In[430]:


temp = temp[temp['bucket1'] != '3 - 0']


# In[431]:


temp.shape


# In[432]:


df3 = df3.groupby(['invoiceNumber']).apply(lambda x : x.drop_duplicates(['buckets'])).reset_index(drop = True)


# In[433]:


df3


# In[434]:


df3['final_dist'] = df3.groupby('invoiceNo')['distance'].diff(-1)*(1)


# In[435]:


df3['final_dist'] = df3['final_dist'].abs()


# In[436]:


df3


# In[437]:


df3['final_time'] = df3.groupby('invoiceNo')['startEntryDatetimestamp'].diff(-1)*(-1)


# In[438]:


df3


# In[439]:


df3['hours'] = df3['final_time']/np.timedelta64(1,'h')


# In[440]:


df3


# In[441]:


len(temp['invoice1'].unique())


# In[442]:


len(df3['invoiceNo'].unique())


# In[443]:


see = pd.merge(df3,temp, left_on =['invoiceNo', 'buckets'], right_on = ['invoice1', 'bucket1'], how = 'left')


# In[444]:


len(see['invoiceNo'].unique())


# In[445]:


see[see['invoiceNo'] == 173056063]


# In[446]:


see['after_hault'] = see['hours']-see['time_sum']


# In[447]:


see


# In[448]:


see['after_hault'] = see['after_hault'].fillna(0)


# In[449]:


see


# In[450]:


for i in range(0, len(see)):
    if see['after_hault'][i] == 0:
        see['after_hault'][i] = see['hours'][i]
        


# In[451]:


see


# In[452]:


temp


# In[453]:


see['speed'] = see['final_dist']/see['after_hault']


# In[454]:


see


# In[455]:


see.drop(['createdDateTime', 'invoiceNumber', 'createDatetimestamp'], axis = 1, inplace = True)


# In[456]:


see


# In[457]:


see = see.groupby(['route', 'buckets'], as_index = False).agg({'final_dist':'mean','after_hault':'mean','speed':'mean', 'invoiceNo': 'count'})


# In[458]:


see= see.rename({'invoiceNo':'trip_count'}, axis = 1)


# In[459]:


see


# In[40]:


#import datetime
#from datetime import datetime
#a =datetime.today().strftime('%Y-%m-%d')
#df3.to_csv('output/report' + ' ' + str(a) + '.csv' )


# In[460]:


import datetime


# In[461]:


from datetime import datetime


# In[462]:


a =datetime.today().strftime('%Y-%m-%d')


# In[463]:


see.to_csv('output/report' + ' ' + str(a) + '.csv' )


# In[ ]:




